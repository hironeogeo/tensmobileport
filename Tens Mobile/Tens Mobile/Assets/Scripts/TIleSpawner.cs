using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TIleSpawner : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject tileToSpwn;

    void Start()
    {
        spawnTile();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void spawnTile()
    {
        int value = Random.Range(0, 6);
        GameObject newTile = Instantiate(tileToSpwn);
        newTile.GetComponent<Tile>().setupTile(value);
        newTile.transform.position = this.transform.position;
        newTile.transform.parent = this.transform.parent;

    }
}
