using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;


public class Tile : MonoBehaviour
{
  public int tileValue;

    Text tileText;
    CanvasGroup canvasGroup;

    public void setupTile(int value)
    {
        tileValue = value;
    }


    // Start is called before the first frame update
    void Start()
    {
        tileText = GetComponent<Text>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if(tileText.text == "")
        {
            tileText.text = tileValue.ToString();
        }
    }



}
