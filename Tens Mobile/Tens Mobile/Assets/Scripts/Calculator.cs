using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Calculator : MonoBehaviour
{
    public GameObject cellA;
    public GameObject cellB;
    public GameObject cellC;
    public GameObject cellD;
    public GameObject cellE;

    private Text totalValue;
    private string cellAVal;
    private string cellBVal;
    private string cellCVal;
    private string cellDVal;
    private string cellEVal;

    private void Awake()
    {
        totalValue = GetComponent<Text>();
        cellAVal = cellA.GetComponent<Text>().text;
        cellBVal = cellB.GetComponent<Text>().text;
        cellCVal = cellC.GetComponent<Text>().text;
        cellDVal = cellD.GetComponent<Text>().text;
        cellEVal = cellE.GetComponent<Text>().text;
    }

    public void updateTotal()
    {
        totalValue.text = cellAVal + cellBVal + cellCVal + cellDVal + cellEVal;
    }




}
