using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GridSlot : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("on drop");
        if(eventData != null)
        {
            GetComponent<Text>().text = eventData.pointerDrag.GetComponent<Text>().text;
            Destroy(eventData.pointerDrag);
        }
    }
}
