using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private RectTransform cellTransform;
    CanvasGroup canvasGroup;

    private void Awake()
    {
        cellTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();

    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("drag event started");
        canvasGroup.alpha = .6f;
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("on drag");
        cellTransform.anchoredPosition += eventData.delta;
        canvasGroup.interactable = false;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("drag event ended");
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("pointer down event");
    }

    //public void OnDrop(PointerEventData eventData)
    //{
    //    Debug.Log("on drop");
    //}
}
